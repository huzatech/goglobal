function myFunction() {
    document.getElementById("panel").style.display = "block";
}

$(document).ready(function(){


    $(".slidingDiv").hide();
	$(".show_hide").show();
	
	$('.show_hide').click(function(){
	$(".slidingDiv").slideToggle();
	});


	// custom 
	$("#box").hide();
	
	//attach click event to buttons
	$(document).on('click', '.button-show', function() { 
		/**
		 * when show button is clicked we call the show plugin
		 * which scales the box to default size
		 * You can try other effects from here: http://jqueryui.com/effect/
		 */
		
		$(this).parents('.results').find("#box").slideToggle();
	});
	
	$(document).on('click', '.close-box', function() {
		$(this).parents('.results').find("#box").slideUp();
	});
	
	$(document).on('click', '.tabs', function(e) {
	  e.preventDefault()
	  $(this).tab('show')
	})
});

	