<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    //return view('welcome');
    return view('hotel-home');
});*/


Route::get('/', 'HotelSearchController@index');

//Route::post('/hotel/search', 'HotelSearchController@search');
Route::match(['get', 'post'], '/hotel/search','HotelSearchController@search');
Route::match(['get', 'post'], '/hotels/search','HotelSearchController@searchHotels');
Route::match(['get', 'post'], '/hotels/search/ajax','HotelSearchController@searchHotelsAjax');


Route::get('/travellanda', 'HotelSearchController@getTravellanda');
Route::get('/hotelbeds', 'HotelSearchController@getHotelbeds');