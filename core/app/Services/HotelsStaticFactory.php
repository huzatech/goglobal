<?php

namespace App\Services;

use App\Contracts\HotelsApiInterface;
use App\Services\TravellandaApi;
use App\Services\HotelbedsApi;

final class HotelsStaticFactory
{
    /**
     * @param string $type
     *
     * @return HotelsApiInterface
     */
    public static function factory(string $type): HotelsApiInterface
    {
        if ($type == 'travellanda') {
            return new TravellandaApi();
        }

        if ($type == 'hotelbeds') {
            return new HotelbedsApi();
        }

        throw new \InvalidArgumentException('Unknown format given');
    }
}