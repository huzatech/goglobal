<?php

namespace App\Services;

use Exception;
use App\Contracts\HotelsApiInterface;
use GuzzleHttp\Client as GuzzleClient;

class TravellandaApi implements HotelsApiInterface
{

    private $username = '';
    private $password = '';
    private $url = '';

    public function __construct()
    {
        $this->username = env('TRV_USERNAME');
        $this->password = env('TRV_PASSWORD');
        $this->url = env('TRV_URL');
    }

    public function getApi($method,$body='')
    {
        try {
            $client = new GuzzleClient();

            /*$body = '<?xml version="1.0" encoding="UTF-8"?><Request><Head><Username>'.$this->username.'</Username><Password>'.$this->password.'</Password
><RequestType>'.$method.'</RequestType></Head><Body/></Request>';*/

            /*$body = '<?xml version="1.0" encoding="UTF-8"?><Request><Head><Username>'.$this->username.'</Username><Password>'.$this->password.'</Password
><RequestType>'.$method.'</RequestType></Head><Body><CountryCode>GB</CountryCode></Body></Request>';*/

            if(empty($body))
                $body = '<CityIds><CityId>117976</CityId></CityIds><CheckInDate>2017-12-18</CheckInDate><CheckOutDate>2017-12-19</CheckOutDate><Rooms><Room><NumAdults>2</NumAdults></Room><Room><NumAdults>1</NumAdults><Children><ChildAge>4</ChildAge><ChildAge>6</ChildAge></Children>
</Room></Rooms><Nationality>FR</Nationality><Currency>GBP</Currency><AvailableOnly>0</AvailableOnly>';


            $body = '<?xml version="1.0" encoding="UTF-8"?><Request><Head><Username>'.$this->username.'</Username><Password>'.$this->password.'</Password
><RequestType>'.$method.'</RequestType></Head><Body>'.$body.'</Body></Request>';

            $request = $client->post($this->url, [
              'body' => [
                'xml' => $body
              ]
            ]);

            if((int)$request->getStatusCode()!==200){
                throw new Exception("A successfully created status code should be returned.");
            }
            //return $request->getBody();
            $xml = simplexml_load_string($request->getBody());
            return json_encode($xml);
            //return json_decode(json_encode($xml),TRUE);


        } catch (\GuzzleHttp\Exception\ClientException $e) {
            //echo 'Caught response: ' . $e->getResponse()->getStatusCode();
            //echo '<br />';
            //echo $responseBodyAsString = $e->getBody()->getContents();
            //echo '<br />';
            //print_r($e);
            dd($e);
            return $e;
        }


    }

  /**
  * Sends a get request to GitHub.
  *
  * @param string                $method
  * @param \App\Models\User\User $user
  * @param string                $route
  * @param array                 $query
  * @param array                 $headers
  *
  * @return mixed|null
  */
  /*protected function json11($method, User $user, $route, $query = [], $headers = [])
  //public function json($method)
  {
    //return 'My method is: '.$method;
     $payload = $method === 'get' ? 'query' : 'json';
    try {
      //$response = app(GuzzleClient::class)->{$method}('https://api.github.com/'.ltrim($route, '/'), [
        $response = app(GuzzleClient::class)->{$method}('http://xmldemo.travellanda.com/xmlv1', [
          'headers' => [
            'Accept' => 'application/vnd.github.moondragon+json',
            'Authorization' => 'token '.$user->token,
          ] + $headers,
        $payload => $query,
      ]);
    } catch (Exception $e) {
      return $e;
    }
    return $response->json();
  }*/
 /**
  * Download a file and store it locally.
  *
  * @param string $token
  * @param string $link
  * @param string $destination
  *
  * @return bool
  */
  /*protected function download($token, $link, $destination)
  {
    if (strpos($link, 'http') !== 0) {
      $link = 'https://api.github.com/'.ltrim($link, '/');
    }
    try {
      $response = app(GuzzleClient::class)->get($link, [
        'save_to' => $destination,
        'headers' => [
          'Authorization' => 'token '.$token,
        ],
      ]);
    } catch (Exception $e) {
      return false;
    }
    return true;
  }*/

 /**
  * Allows for magic :method methods for Http verbs.
  *
  * @param string $method
  * @param array  $args
  *
  * @return mixed
  */
  /*public function __call($method, $args)
  {
    if (!in_array($method, ['get', 'post', 'put', 'patch', 'delete', 'options'])) {
      throw new Exception("Method {$method} doesn't exist!");
    }
    array_unshift($args, $method);
    return call_user_func_array([$this, 'json'], $args);
  }*/
}