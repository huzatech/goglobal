<?php

namespace App\Services;

use Exception;
use App\Contracts\HotelsApiInterface;
use GuzzleHttp\Client as GuzzleClient;

class HotelbedsApi implements HotelsApiInterface
{
    private $api_key = '';
    private $x_signature = '';
    private $url = '';

    public function __construct()
    {
        $this->api_key = $_ENV['HB_API_KEY'];
        $this->x_signature = hash("sha256", $_ENV['HB_API_KEY'].$_ENV['HB_SECRET'].time());
        $this->url = $_ENV['HB_URL'];
    }

    public function getApi($query)
    {
        try {
            $client = new GuzzleClient();
            /*$request = $client->get('https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels?fields=all&language=ITA&from=12&to=20', [
              'headers' => [
                'Api-Key' 			=> 'a9pcebjz62j7w2yzm98x4s2m',
                'X-Signature'   => '24bd55a5533f9e045a504ad6e9d35e5c9e80014fbd4dd82686773f4a3f5ab3ab',
              ]
            ]);*/
            $request = $client->get('https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels?fields=all&language=ITA&from=12&to=20', [
              'headers' => [
                'Api-Key' 			=> $this->api_key,
                'X-Signature'   => $this->x_signature,
              ]
            ]);

            //echo $request->getStatusCode();
            //echo $request->getBody();
            return $request->getBody();

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo 'Caught response: ' . $e->getResponse()->getStatusCode();
            echo '<br />';
            //echo $responseBodyAsString = $e->getBody()->getContents();
            echo '<br />';
            print_r($e);
        }
    }
}