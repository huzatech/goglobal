<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use App\Contracts\HotelsApiInterface;
use App\Services\HotelsStaticFactory;

//use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illluminate\Support\Collection;
use Illuminate\Pagination\Paginator;

class HotelSearchController extends Controller
{
  /**
   * Hotel index page.
   *
   * @param  Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    //$url = $request->url();
    $url = url('/');
    $appName = config('app.name');
    return view('hotel-home', ['url' => $url, 'appName' => $appName]);
  }

  /**
 * Hotel search page.
 *
 * @param  Request $request
 * @return Response
 */
  public function search(Request $request)
  {
    $url = url('/');
    $appName = config('app.name');

    $hotels = HotelsStaticFactory::factory('travellanda')->getApi('HotelSearch');

    $paginator = new Paginator($hotels['Body']['Hotels']['Hotel'], '10');
    $paginator->withPath('search');

    /*$paginator = new Paginator($hotels['Body']['Hotels']['Hotel'], '10', '10', 'page', [
      'path'  => $request->url(),
      'query' => $request->query(),
    ]);*/

    $hotelIds = '';
    //dd(count($hotels['Body']['Hotels']['Hotel']));
    foreach($hotels['Body']['Hotels']['Hotel'] as $key=>$hotel) {
      // Only send max 500 HotelId
      if($key>9)
        break;
      $hotelIds .= '<HotelId>'.$hotel['HotelId'].'</HotelId>';
    }

    //dd($hotelIds);
    $body = '<HotelIds>'.$hotelIds.'</HotelIds>';
    $hotelsDetails = HotelsStaticFactory::factory('travellanda')->getApi('GetHotelDetails', $body);
    //dd([$hotels,$hotelsDetails]);

    return view('hotel-search', ['url' => $url,
      'appName' => $appName,
      'hotels' => $hotels,
      'paginator' => $paginator,
      'hotelsDetails' => $hotelsDetails]);
  }

  /**
   * Hotel search page.
   *
   * @param  Request $request
   * @return Response
   */
  public function searchHotels(Request $request)
  {
    $url = url('/');
    $appName = config('app.name');
    return view('hotels-search', ['url' => $url, 'appName' => $appName]);
  }

  /**
   * Hotel search.
   *
   * @param  Request $request
   * @return Response
   */
  public function searchHotelsAjax(Request $request)
  {
    $hotels = HotelsStaticFactory::factory('travellanda')->getApi('HotelSearch');
    return response($hotels);
  }

  /**
   * Hotel images search.
   *
   * @param  Request $request
   * @return Response
   */
  public function searchHotelsImagesAjax($hotelIds)
  {
    $hotelIdsXml = '';
    foreach($hotelIds as $key=>$hotel) {
      // Only send max 500 HotelId
      if($key>9)
        break;
      $hotelIdsXml .= '<HotelId>'.$hotel['HotelId'].'</HotelId>';
    }

    $body = '<HotelIds>'.$hotelIdsXml.'</HotelIds>';
    $hotelsDetails = HotelsStaticFactory::factory('travellanda')->getApi('GetHotelDetails', $body);
    return response($hotelsDetails);
  }

  public function getTravellanda()
  {
     //return response($interactor->getApi('GetHotels'))->header('Content-Type', 'text/plain');
     //return view('travellanda')->with('data', TravellandaApi::getApi('GetHotels'));
    $body = '';
    return view('travellanda')->with('data', HotelsStaticFactory::factory('travellanda')->getApi('HotelSearch', $body));
  }

  public function getHotelbeds()
  {
      //return view('hotelbeds');
      return view('hotelbeds')->with('data', HotelsStaticFactory::factory('hotelbeds')->getApi('GetHotels'));
  }

}
