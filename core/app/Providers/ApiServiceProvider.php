<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class ApiServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot()
    {
        //
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        //
        /*$this->app->bind(Travellanda::class, function ($app) {
            return new Travellanda();
        });*/

        $this->app->bind(
          'App\Contracts\HotelsApiInterface',
          'App\Services\HotelsStaticFactory'
        );

        /*$this->app->singleton(StaticFactory::class, function ($app) {
            return new StaticFactory();
        });*/
    }
}
