@extends('layouts.master-inner')

@section('title', 'Search')

<!-- navbar start -->
@section('navbar')
    <div class="container">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header navbar-static-top">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="{{$url}}"><img src="{{$url}}/assets/img/weblogo2.png" width="200" height="69"></a> </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- Trigger/Open The Modal -->
            </div>

            <!-- Button trigger modal -->

            <div class="account" data-toggle="modal" data-target="#myModal">
                <div class="image"><img src="{{$url}}/assets/img/account.png" width="16" height="16"></div>
                <div class="paragraph">My Account</div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content"> <span class="glyphicon glyphicon-triangle-top"></span>
                        <ul>
                            <li>
                                <button type="button" class="default-button button-orange">Sign In</button>
                            </li>
                            <li>
                                <button type="button" class="default-button button">Sign Up</button>
                            </li>
                            <div class="items">
                                <!--  <ul>
                    <li>Trips</li>
                    <li>Price Alerts</li>
                    </ul>-->
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
@endsection('navbar')
<!-- navbar end -->

<!-- left-side start -->
@section('left-side')
    <div id="filtertab-s" class="filtertab-s">
        <div id="topfiltertab" class="filtertab selectedtab">Top Filters</div>
        <div id="tabindicator" class="indicator"></div>
    </div>
    <div id="hotel-name" class="hotel-name">
        <h5>Hotel Name</h5>
        <input type="text" id="myInput" placeholder="Search for names.." title="Type in a name">
    </div>
    <div id="range-slider" class="range-slider"> <span id="price-text"> Price Range </span>
    <span id="price-range">
    <div class="min">$20</div>
    <div class="dash">-</div>
    <div class="max">10,000</div>
    </span>
        <div id="slider"></div> </div>
    <div class="border-top"></div>
    <div id="star-ranking" class="star-ranking"> <span id="hotel-rating"> Star </span>
        <input id="input-4" name="input-4" class="rating rating-loading" data-show-clear="false" data-show-caption="true">
        <div class="rating rating-loading"></div>
    </div>
    <div class="board-type"> <span id="text">Board Type</span>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="">
                Room only</label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="">
                Bed & Breakfast</label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="">
                Half Board</label>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="">
                Full Board</label>
        </div>
    </div>
    <div id="availability" class="availability"> <span id="text">Availability</span>
        <div class="checkbox">
            <label>
                <input type="checkbox" value="">
                Available only</label>
        </div>
    </div>
    <div id="points-of-interest" class="points-of-interest">
        <div class="filter"> <span id="text">Point of Interest</span></div>
        <span id="text">Airports</span></div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Al Maktoum International (DWC)</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Dubai International Airport (DXB)</label>
    </div>
    <span id="text">Area</span>
    <div class="overflow">
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Al-Barsha</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Al Garhoud</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Al Qusais / Al Gusais</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Al Sufouh</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Al Wasl</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Barsha Heights (Tecom)</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Bur Dubai</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Burj Al Arab tower</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Burj Khalifa</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Business Bay</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Deira</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Downtown Dubai</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Dubai Creek</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Dubai Desert</label>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            Dubai Investment Park</label>
    </div>
</div>
@endsection('left-side')
<!-- left-side end -->

<!-- right-side start -->
@section('right-side')
    <!-- Reservation Form-->
    <div class="slidingDiv">
        <div class="clearfix" id="reservation-form">
            <form class="form-inline reservation-horizontal clearfix">
                <div class="form-group">
                    <label for="email" accesskey="E"></label>
                    <input name="email" type="text" id="email" value="" class="form-control" placeholder="Destination">
                </div>
                <div class="form-group">
                    <label for="checkin"></label>
                    <input name="checkin" type="text" class="form-control datepicker hasDatepicker" placeholder="Check-in" id="dp1489401866411">
                </div>
                <div class="form-group">
                    <label for="checkout"></label>
                    <input name="checkout" type="text" value="" class="form-control datepicker hasDatepicker" placeholder="Check-out" id="dp1489401866411">
                </div>
                <div class="form-group">
                    <div class="guests-select flip" onClick="myFunction()">
                        <div class="total form-control" id="test"><span class="para">1 room 1 guest</span></div>
                        <div id="panel">
                            <div class="guests-select1"> <span class="glyphicon1 glyphicon-triangle-top"></span>
                                <div class="room">
                                    <div class="col-md-4">Room</div>
                                    <div class="col-md-8"> <img src="{{$url}}/assets/img/minus-sign-inside-a-black-rounded-square-shape.png" width="32" height="32">&nbsp;&nbsp;
                                        1&nbsp;&nbsp; <img src="{{$url}}/assets/img/plus-symbol-in-a-rounded-black-square.png" width="32" height="32"> </div>
                                </div>
                                <div class="room1">
                                    <div class="col-md-4">Guest</div>
                                    <div class="col-md-8"> <img src="{{$url}}/assets/img/minus-sign-inside-a-black-rounded-square-shape.png" width="32" height="32">&nbsp;&nbsp;
                                        1&nbsp;&nbsp; <img src="{{$url}}/assets/img/plus-symbol-in-a-rounded-black-square.png" width="32" height="32"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button class="btnCustom btnCustom-default">SEARCH</button>
                </div>
            </form>
        </div>
    </div>
    <div id="change-res-form" class="change-res-form">
        <div class="change-search">
            <div class="search-results"> <span id="some-text"><a class="show_hide">Dubai, United Arab Emirates</a></span> <span id="right-border"><img src="{{$url}}/assets/img/line.PNG" class="img-responsive"></span> <span id="some-text"><a class="show_hide">Apr 15 - Apr 18</a></span> <span id="right-border"><img src="{{$url}}/assets/img/line.PNG" class="img-responsive"></span> <span id="some-text"><a class="show_hide">1 Room 2 Guests</a></span> <span id="right-border"><img src="{{$url}}/assets/img/line.PNG" class="img-responsive"></span> </div>
        </div>
        <div class="show-button-orange">
            <div class="show_hide">Change Search</div>
        </div>
    </div>
    <!-- Reservation Form End-->
    <div id="margin"></div>
    <div id="result" class="hotels-search">
        <div id="loadingDiv">
            <img id="loadingGif" src="{{$url}}/js/images/loading32x32.gif" alt="loading ..." />
            <div>
                <h7>Please wait...</h7>
            </div>
            <input type="hidden" id="show_more_total" />
        </div>
        {{-- <div class="results">

                 <div class="left-image"></div>
                 <div class="right-content"></div>
                 <div class="clearfix"></div>
                 <div id="tabs" class="tabs clearfix">
                     <div id="box" class="box"> <span class="close-box">×</span>
                         <!-- Nav tabs -->
                         <ul class="nav nav-tabs" role="tablist">
                             <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
                             <li role="presentation"><a href="#map" aria-controls="map" role="tab" data-toggle="tab">Map</a></li>
                             <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>

                         </ul>

                         <!-- Tab panes -->
                         <div class="tab-content">
                             <div role="tabpanel" class="tab-pane fade in active" id="details">
                                 <p>Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.Excellent business hotel. Close to Dubai Airport.
                                     Located near shopping areas and the airport. Great room. Popular among solo travelers.</p>
                             </div>
                             <div role="tabpanel" class="tab-pane fade" id="map"> <img src="{{$url}}/assets/img/map.jpg" class="img-responsive">
                                 <div class="right-panel-map">
                                     <div class="text-center address">P.O Box 35566, Dubai</div>
                                     <div class="phone text-center">+971 4224 4000</div>
                                     <div class="nearby">
                                         <ul class="nearby-list">
                                             <li><span class="pull-left">Some-City</span><span class="pull-right">0.18 mi</span></li>
                                             <li><span class="pull-left">Some-City</span><span class="pull-right">0.18 mi</span></li>
                                             <li><span class="pull-left">Some-City</span><span class="pull-right">0.18 mi</span></li>
                                             <li><span class="pull-left">Some-City</span><span class="pull-right">0.18 mi</span></li>
                                         </ul>
                                     </div>
                                 </div>
                             </div>
                             <div role="tabpanel" class="tab-pane fade" id="messages">...</div>

                         </div>
                     </div>
                 </div>

        </div>--}}
    </div>
@endsection('right-side')
<!-- right-side end -->
