@extends('layouts.master')

@section('title', 'Home')

<!-- search start -->
@section('search')
    <div class="heading">   <h2>Search hundreds of hotel sites at once</h2></div>
    <section id="">
        <div class="container">
            <div class="">
                <div class="row container">
                    <div class="col-md-12">
                        <form class="form-inline reservation-horizontal clearfix" method="post" action="{{$url}}/hotels/search">
                            {{ csrf_field() }}
                            <div class="row container">
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <label for="email" accesskey="E"></label>
                                        <input name="email" type="text" id="email" value="" class="form-control" placeholder="Destination">
                                    </div>
                                </div>

                                <div class="col-sm-2 nopadding">
                                    <div class="form-group">
                                        <label for="checkin"></label>
                                        <input name="checkin" type="text" class="form-control datepicker" placeholder="Check-in">
                                    </div>
                                </div>
                                <div class="col-sm-2 nopadding">
                                    <div class="form-group">
                                        <label for="checkout"></label>
                                        <input name="checkout" type="text" value="" class="form-control datepicker" placeholder="Check-out">
                                    </div>
                                </div>
                                <div class="col-sm-3 nopadding">
                                    <div class="form-group">
                                        <div class="guests-select flip" onClick="myFunction()">
                                            <div class="total form-control" id="test"><span class="para">1 room 1 guest</span></div>



                                            <div id="panel">
                                                <div class="guests-select1">
                                                    <span class="glyphicon1 glyphicon-triangle-top"></span>
                                                    <div class="room">
                                                        <div class="col-md-4">Room</div>
                                                        <div class="col-md-8">
                                                            <img src="{{$url}}/assets/img/minus-sign-inside-a-black-rounded-square-shape.png" width="32" height="32">&nbsp;&nbsp;
                                                            1&nbsp;&nbsp;
                                                            <img src="{{$url}}/assets/img/plus-symbol-in-a-rounded-black-square.png" width="32" height="32">
                                                        </div></div>
                                                    <div class="room1">
                                                        <div class="col-md-4">Guest</div>
                                                        <div class="col-md-8">
                                                            <img src="{{$url}}/assets/img/minus-sign-inside-a-black-rounded-square-shape.png" width="32" height="32">&nbsp;&nbsp;
                                                            1&nbsp;&nbsp;
                                                            <img src="{{$url}}/assets/img/plus-symbol-in-a-rounded-black-square.png" width="32" height="32">
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btnCustom btnCustom-default">SEARCH</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div></div></div>
    </section>
@endsection('search')
<!-- search end -->

<!-- search sites start -->
@section('search-sites')
    <div class="row">
        <div class="text animatedParent">
            <h4 class=" animated bounceInRight">We’re searching for your perfect hotel on these sites & hundreds more.</h4>
        </div>
        <div class="hotel-main">
            <div class="col-md-4 col-sm-12">
                <div class="hotel-container">
                    <div class="imgWrap">
                        <div class="imgDescription">
                            <p>Australia<br>
                            <h4>Sydney </h4><br>
                            4 Days<br>$1000</p>
                            <button type="submit" class="btn btn-primary btn-block">Read More</button>
                        </div>
                        <img src="{{$url}}/assets/img/Sydney Opera House.jpg" class="img-responsive" alt="polaroid" />

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="hotel-container">
                    <div class="imgWrap">
                        <div class="imgDescription">
                            <p>Central America<br>
                            <h4>Mexico </h4><br>
                            4 Days<br>$1000</p>
                            <button type="submit" class="btn btn-primary btn-block">Read More</button>
                        </div>
                        <img src="{{$url}}/assets/img/69380091-uk-wallpapers.jpeg" class="img-responsive" alt="polaroid" />

                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="hotel-container">
                    <div class="imgWrap">
                        <div class="imgDescription">
                            <p>Europe<br>
                            <h4>England </h4><br>
                            4 Days<br>$1000</p>
                            <button type="submit" class="btn btn-primary btn-block">Read More</button>
                        </div>
                        <img src="{{$url}}/assets/img/69380091-uk-wallpapers.jpeg" class="img-responsive" alt="polaroid" />

                    </div>
                </div>
            </div></div>
    </div>
@endsection('search-sites')
<!-- search sites end -->

<!-- travel planning start -->
@section('travel-planning')
    <div class="text-center animatedParent">
        <h2 class="animated bounceInLeft">Start your travel planning here</h2>
        <h4 class=" animated bounceInRight">Find Hotels</h4>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="view view-tenth">
            <img src="{{$url}}/assets/img/5587273-travel-wallpapers.jpg" class="img-responsive">
            <div class="mask">
                <h2>NEW YORK</h2>
                <p>Lorem Ipsum.</p>
                <h2>Special Deal</h2>
                <h3>4 Days<br>$1000</h3>
                <a href="#" class="info">Read More</a>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 animatedParent">
        <div class="view view-tenth">
            <img src="{{$url}}/assets/img/travel.jpg" class="img-responsive">
            <div class="mask">
                <h2>Somewhere Beautiful</h2>
                <p>Lorem Ipsum.</p>
                <h2>Special Deal</h2>
                <h3>4 Days<br>$1000</h3>
                <a href="#" class="info">Read More</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 animatedParent">
        <div class="view view-tenth">
            <img src="{{$url}}/assets/img/canada.jpg" class="img-responsive">
            <div class="mask">
                <h2>NEW YORK</h2>
                <p>Lorem Ipsum.</p>
                <h2>Special Deal</h2>
                <h3>4 Days<br>$1000</h3>
                <a href="#" class="info">Read More</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 animatedParent">
        <div class="view view-tenth">
            <img src="{{$url}}/assets/img/canada.jpg" class="img-responsive">
            <div class="mask">
                <h2>NEW YORK</h2>
                <p>Lorem Ipsum.</p>
                <h2>Special Deal</h2>
                <h3>4 Days<br>$1000</h3>
                <a href="#" class="info">Read More</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 animatedParent">
        <div class="view view-tenth">
            <img src="{{$url}}/assets/img/canada.jpg" class="img-responsive">
            <div class="mask">
                <h2>NEW YORK</h2>
                <p>Lorem Ipsum.</p>
                <h2>Special Deal</h2>
                <h3>4 Days<br>$1000</h3>
                <a href="#" class="info">Read More</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 animatedParent">
        <div class="view view-tenth">
            <img src="{{$url}}/assets/img/canada.jpg" class="img-responsive">
            <div class="mask">
                <h2>NEW YORK</h2>
                <p>Lorem Ipsum.</p>
                <h2>Special Deal</h2>
                <h3>4 Days<br>$1000</h3>
                <a href="#" class="info">Read More</a>
            </div>
        </div>
    </div>
@endsection('travel-planning')
<!-- travel planning end -->

<!-- testimonials start -->
@section('testimonials')
    <div class="testimonials-container section-container animatedParent">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 testimonials section-description text-center animated bounceInDown">
                            <h2>Our Testimonials</h2>
                            <div class="divider-1"><div class="line"></div></div>
                            <p class="medium-paragraph">Take a look below to learn what our clients are saying about us:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 testimonial-list">
                            <div role="tabpanel">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="tab1">
                                        <div class="testimonial-image">
                                            <img src="{{$url}}/assets/img/oval-face copy.png" alt="t1">
                                        </div>
                                        <div class="testimonial-text">
                                            <p>
                                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                                Lorem ipsum dolor sit amet, consectetur..."<br>
                                                <a href="#">Lorem Ipsum</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab2">
                                        <div class="testimonial-image">
                                            <img src="{{$url}}/assets/img/testimonials/2.jpg" alt="t2">
                                        </div>
                                        <div class="testimonial-text">
                                            <p>
                                                "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip
                                                ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                                                lobortis nisl ut aliquip ex ea commodo consequat..."<br>
                                                <a href="#">Minim Veniam,</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab3">
                                        <div class="testimonial-image">
                                            <img src="{{$url}}/assets/img/testimonials/3.jpg" alt="t3">
                                        </div>
                                        <div class="testimonial-text">
                                            <p>
                                                "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.
                                                Lorem ipsum dolor sit amet, consectetur..."<br>
                                                <a href="#">Lorem Ipsum</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab4">
                                        <div class="testimonial-image">
                                            <img src="{{$url}}/assets/img/testimonials/4.jpg" alt="t4">
                                        </div>
                                        <div class="testimonial-text">
                                            <p>
                                                "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip
                                                ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                                                lobortis nisl ut aliquip ex ea commodo consequat..."<br>
                                                <a href="#">Minim Veniam,</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection('testimonials')
<!-- testimonials end -->

<!-- signup start -->
@section('signup')
    <form action="#" method="post" name="sign up for beta form">
        <div class="header">
            <p>Sign Up </p>
        </div>
        <div class="description">
            <p>Lorem Ipsum dolor sit amet. If you're interested in testing it out, then sign up below to get exclusive access.</p>
        </div>
        <div class="input">
            <input type="text" class="button1" id="email1" name="email" placeholder="NAME@EXAMPLE.COM">
            <input type="submit" class="button1" id="submit1" value="SIGN UP">
        </div>
    </form>
@endsection('signup')
<!-- signup end -->

<!-- why-with-us start -->
@section('why-with-us')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="why-us-container">
                    <div class="icon-suit">
                        <img src="{{$url}}/assets/img/baggage-icon-png-1.png" width="32" height="32">
                    </div>
                    <h3>TRAVEL INSURANCE</h3>
                    <p>Travel Insurance for your favorite international destinations,  get quotes and deals on the best travel insurance with GOGLOBAL</p>
                    <div class="default-button">
                        <button type="button" class="default-button button-orange">Read More</button></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="why-us-container"><div class="icon-suit">
                        <img src="{{$url}}/assets/img/ship.png" width="32" height="32">
                    </div>
                    <h3>CRUISE BOOKING</h3>
                    <p>Travel Insurance for your favorite international destinations,  get quotes and deals on the best travel insurance with GOGLOBAL</p>
                    <div class="default-button">
                        <button type="button" class="default-button button-orange">Read More</button></div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="why-us-container"><div class="icon-suit">
                        <img src="{{$url}}/assets/img/island-with-palm-trees.png" width="32" height="32">
                    </div>
                    <h3>HOLIDAYS</h3>
                    <p>Travel Insurance for your favorite international destinations,  get quotes and deals on the best travel insurance with GOGLOBAL</p>
                    <div class="default-button">
                        <button type="button" class="default-button button-orange">Read More</button></div>
                </div>

            </div>
        </div>
    </div>
@endsection('why-with-us')
<!-- why-with-us end -->

