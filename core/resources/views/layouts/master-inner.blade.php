<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @php
    //echo '<pre>';
    //print_r($_REQUEST);
    //echo '</pre>';

    @endphp

    <meta name="csrf-token" content="@php echo $_POST['_token'] ?? csrf_token();@endphp">
    <title>@yield('title') - {{$appName}}</title>
    <link rel="stylesheet" href="{{$url}}/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/animations.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/user.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/custom-search.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/responsive.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/date-picker.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/style.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/search-result.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/nouislider.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/star-rating.css">


    <style>
        #loadingDiv {
            display:none;
            position:fixed;
            top:0px;
            right:0px;
            width:100%;
            height:100%;
            background-color:#666;
            /*background-image:url('ajax-loader.gif');*/
            background-repeat:no-repeat;
            background-position:center;
            z-index:1000;
            opacity: 0.4;
            filter: alpha(opacity=40); /* For IE8 and earlier */
        }
        #loadingGif {
            left : 50%;
            top : 50%;
            position : absolute;
            z-index : 1001;
            width : 32px;
            height : 32px;
            margin-left : -16px;
            margin-top : -16px;
        }
    </style>

</head>

<body>


<nav class="navbar navbar-default navbar-static-top">
    @yield('navbar')
</nav>

<div class="wrapper row">
    <div class="left-side">
        @yield('left-side')
    </div>
    <div class="right-side">
        @yield('right-side')
    </div>
</div>


<div class="splitter"></div>



<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 footerleft ">
                <div class="logofooter"> <img src="{{$url}}/assets/img/logo-white.png" class="img-responsive"></div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                <p><i class="fa fa-map-pin"></i> 210 Lorem ipsum dolor -        112233, Pakistan</p>
                <p><i class="fa fa-phone"></i> Phone 03000-12-34-56</p>
                <p><i class="fa fa-envelope"></i> E-mail : info@huzatech.com</p>
            </div>
            <div class="col-md-2 col-sm-6 paddingtop-bottom">
                <h6 class="heading7">GENERAL LINKS</h6>
                <ul class="footer-ul">
                    <li><a href="#"> Career</a></li>
                    <li><a href="#"> Privacy Policy</a></li>
                    <li><a href="#"> Terms & Conditions</a></li>
                    <li><a href="#"> Client Gateway</a></li>
                    <li><a href="#"> Ranking</a></li>
                    <li><a href="#"> Case Studies</a></li>
                    <li><a href="#"> Frequently Ask Questions</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 paddingtop-bottom">
                <h6 class="heading7">LATEST POST</h6>
                <div class="post">
                    <p>Lorem ipsum dolor amit set <span>August 3,2015</span></p>
                    <p>Lorem ipsum dolor amit set <span>August 3,2015</span></p>
                    <p>Lorem ipsum dolor amit set <span>August 3,2015</span></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 paddingtop-bottom">
                <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-height="300"
                     data-small-header="false" style="margin-bottom:15px;" data-adapt-container-width="true" data-hide-cover="false"
                     data-show-facepile="true">
                    <div class="fb-xfbml-parse-ignore">
                        <blockquote cite="https://www.facebook.com/facebook"><a
                                    href="https://www.facebook.com/facebook">Facebook</a></blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="col-md-6">
            <p>&copy; {{date('Y')}} - All Rights with HuzaTech</p>
        </div>
        <div class="col-md-6">
            <ul class="bottom_ul">
                <li><a href="{{$url}}">GoGlobal.com</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Faq's</a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Site Map</a></li>
            </ul>
        </div>
    </div>
</div>



<script src="{{$url}}/assets/js/jquery.min.js"></script>
<script src="{{$url}}/assets/js/jquery-1.11.1.min.js"></script>
<script src="{{$url}}/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="{{$url}}/assets/js/nouislider.min.js"></script>
<script src="{{$url}}/assets/js/add.js"></script>
<script src="{{$url}}/assets/js/reservationjq.js"></script>
<script src="{{$url}}/assets/js/customJs.js"></script>
<script src="{{$url}}/assets/js/css3-animate-it.js"></script>
<script src="{{$url}}/assets/js/star-rating.js"></script>
<script>

    var slider = document.getElementById('slider');
    noUiSlider.create(slider, {
        start: [20, 80],
        connect: true,
        range: {
            'min': 0,
            'max': 100
        }
    });

</script>

<script src="{{$url}}/js/hotels-search.js"></script>

</body>
</html>
