<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <title>@yield('title') - {{$appName}}</title>
    <link rel="stylesheet" href="{{$url}}/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/animations.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/user.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/custom-search.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/responsive.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="{{$url}}/assets/css/style.css">
</head>

<body>
<div class="jumbotron hero">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header navbar-static-top">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{$url}}"><img src="{{$url}}/assets/img/weblogo2.png" width="200" height="69"></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">Link</a></li>
                                <li><a href="#">Link</a></li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <!-- Trigger/Open The Modal -->
                        </div>

                        <!-- Button trigger modal -->

                        <div class="account" data-toggle="modal" data-target="#myModal">
                            <div class="image"><img src="{{$url}}/assets/img/account.png" width="16" height="16"></div>
                            <div class="paragraph">My Account</div>

                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">

                                <div class="modal-content">
                                    <span class="glyphicon glyphicon-triangle-top"></span>
                                    <ul>
                                        <li><button type="button" class="default-button button-orange">Sign In</button></li>
                                        <li><button type="button" class="default-button button">Sign Up</button></li>
                                        <div class="items">
                                            <!--  <ul>
                                              <li>Trips</li>
                                              <li>Price Alerts</li>
                                              </ul>-->
                                        </div></ul>
                                </div>
                            </div>
                        </div>





                    </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->

        </div>
    </div>

    <div class="clearfix" id="reservation-form">
        @yield('search')
    </div>
</div>

<div class="container">
    @yield('search-sites')
</div>

<div class="container">
    @yield('travel-planning')
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 testimonialss">
    @yield('testimonials')
</div>

<div class="container">
    @yield('signup')
</div>

<div class="why-with-us">
    @yield('why-with-us')
</div>

<div class="splitter"></div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 footerleft ">
                <div class="logofooter"> <img src="{{$url}}/assets/img/logo-white.png" class="img-responsive"></div>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                <p><i class="fa fa-map-pin"></i> 210 Lorem ipsum dolor -        112233, Pakistan</p>
                <p><i class="fa fa-phone"></i> Phone 03000-12-34-56</p>
                <p><i class="fa fa-envelope"></i> E-mail : info@huzatech.com</p>

            </div>
            <div class="col-md-2 col-sm-6 paddingtop-bottom">
                <h6 class="heading7">GENERAL LINKS</h6>
                <ul class="footer-ul">
                    <li><a href="#"> Career</a></li>
                    <li><a href="#"> Privacy Policy</a></li>
                    <li><a href="#"> Terms & Conditions</a></li>
                    <li><a href="#"> Client Gateway</a></li>
                    <li><a href="#"> Ranking</a></li>
                    <li><a href="#"> Case Studies</a></li>
                    <li><a href="#"> Frequently Ask Questions</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 paddingtop-bottom">
                <h6 class="heading7">LATEST POST</h6>
                <div class="post">
                    <p>Lorem ipsum dolor amit set <span>August 3,2015</span></p>
                    <p>Lorem ipsum dolor amit set <span>August 3,2015</span></p>
                    <p>Lorem ipsum dolor amit set <span>August 3,2015</span></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 paddingtop-bottom">
                <h4>Follow Us</h4>
                <div class="social-network">

                    <div class="social-icons"><img src="{{$url}}/assets/img/youtube-logo.png"></div>
                    <div class="social-icons"><img src="{{$url}}/assets/img/facebook-logo (1).png"></div>
                    <div class="social-icons"><img src="{{$url}}/assets/img/instagram.png"></div>
                    <div class="social-icons"><img src="{{$url}}/assets/img/twitter-logo-button.png"></div>
                    <div class="social-icons"><img src="{{$url}}/assets/img/linkedin-sign.png"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copyright">
    <div class="container">
        <div class="col-md-6">
            <p>&copy; {{date('Y')}} - All Rights with HuzaTech</p>
        </div>
        <div class="col-md-6">
            <ul class="bottom_ul">
                <li><a href="{{$url}}">GoGlobal.com</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Faq's</a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Site Map</a></li>
            </ul>
        </div>
    </div>
</div>


<script src="{{$url}}/assets/js/jquery-1.11.1.min.js"></script>
<script src="{{$url}}/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="{{$url}}/assets/js/jquery-ui.js"></script>
<script src="{{$url}}/assets/js/modal.js"></script>
<script src="{{$url}}/assets/js/reservationjq.js"></script>
<script src="{{$url}}/assets/js/display-block.js"></script>
<script src="{{$url}}/assets/js/css3-animate-it.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".datepicker").datepicker();
    });
</script>

</body>
</html>
