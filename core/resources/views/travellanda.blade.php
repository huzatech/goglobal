data: 123

@php
    $data = json_decode($data);
    echo '<pre>';
    print_r($data);
    dd($data);
    echo '</pre>';

@endphp
<br /><br />
@if(isset($data['Body']['Hotels']) && is_array($data['Body']['Hotels']))
    @foreach ($data['Body']['Hotels']['Hotel'] as $values)
        @foreach ($values as $key => $value)
            Key: {{ $key }}
            Value: {{ $value }}
            <br />
        @endforeach
    @endforeach
@else
    @foreach ($data['Body']['Error'] as $error)
        ErrorId: {{ $error['ErrorId'] }}
        ErrorText: {{ $error['ErrorText'] }}
            <br />
    @endforeach
@endif