$(document).ready(function() {
    
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
	});


	function loadData(page, file){
		
		var title = '';
		var currentToken = $('meta[name="csrf-token"]').attr('content');
		//data: {_token:currentToken,title: title},

		$.ajax({
	        type: "POST",
	        url:  'search/ajax',
	        data: {title: title},
	        beforeSend: function(){
	        	$("#result #loadingDiv").show();
			},
			complete: function(){
				$("#result #loadingDiv").hide();
		    },
	        success: function( msg ) {
		       	//var json = $.parseJSON(msg);
	        	var data = JSON.parse(msg);
	        	console.log(data);
	        	//console.log(data.Body);
	        	//console.log(data.Body.Hotels);
	        	//console.log(data.Body.Hotels.Hotel);
	        	console.log(data.Body.Hotels.Hotel[0].HotelId);
	        	//console.log(data.Body.Hotels.Hotel);
	        	
	        	var html = '';
	        	$.each( data.Body.Hotels.Hotel, function( key, value ) {
				  //console.log( key + ": " + value.HotelId );
				});

	        	$.each( data.Body.Hotels.Hotel, function( key, hotel ) {	
	        		html += '<div class="results results-'+key+'"><div class="left-image">'+
		        				'<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">'+
		        			    '<div class="carousel-inner" role="listbox">'+
		        			    '<div class="item active"><img src="/assets/img/Burj.jpg" class="img-responsive" alt="...">'+
		        			    '<div class="carousel-caption"></div>'+
		        			    '</div><div class="item"><img src="/assets/img/8743e0c5-city-14305-15a94c9ea37.jpg" class="img-responsive" alt="...">'+
		        			    '<div class="carousel-caption"></div></div></div>'+
		        			    '<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">'+
		        			    '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span></a>'+
		        			    '<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">'+
		        			    '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>'+
		        			    '</div></div><div class="right-content">'+ 
		                		'<a id="texty" class="button-show" target="_blank">'+hotel.HotelName+'</a>'+
		                		'<span id="ranking"><img src="/assets/img/star.png" class="img-responsive"></span>'+
		                		'<span id="reviews"> Excellent (8.3, 6,695 reviews)</span>'+
		                        '<div class=" table-style">'+
		                  		'<table>'+
		                    	'<tr class="table-tr1">'+
		                      	'<td>Room Type</td>'+
		                      	'<td>Meal</td>'+
		                      	'<td>Availability</td>'+
		                      	'<td>Rate</td>'+
			                    '</tr>';
			            
			            
			            var i = 1;
			            $.each( hotel.Options.Option, function( key1, option ) {
			            	var flag = true;
			            	if (typeof option.Rooms !== 'undefined' && option.Rooms instanceof Object) {
			            		$.each( option.Rooms.Room, function( key2, room ) {
			            			if (parseInt(i)>4) {
							            flag = false;
							            return false;
							        }
			      					
			      					var available = (parseInt(option.OnRequest)===1)?"On Request":"Available";
				            		html += '<tr class="table-bordered">'+
				                      '<td>'+room.RoomName+'</td>'+
				                      '<td>'+option.BoardType+'</td>'+
				                      '<td>'+available+'</td>'+
				                      '<td>'+data.Body.Currency+' '+room.RoomPrice+'</td>'+
				                      '<td><a href="#">Book Now</a></td>'+
				                    '</tr>';
				                    i++;
			                	});
			                } else {
			                	//$('#result results-'+key+'').hide();		
			                }
			                return flag; 	
			             });       

			            var htmla = '';	
			            htmla += '<tr class="table-bordered">'+
			                      '<td>Some-Type</td>'+
			                      '<td>Meal-Type</td>'+
			                      '<td>Available</td>'+
			                      '<td>£300</td>'+
			                      '<td><a href="#">Book Now</a></td>'+
			                    '</tr>'+
			                    '<tr class="table-bordered">'+
			                      '<td>Some-Type</td>'+
			                      '<td>Meal-Type</td>'+
			                      '<td>Available</td>'+
			                      '<td>£300</td>'+
			                      '<td><a href="#">Book Now</a></td>'+
			                    '</tr>'+
			                    '<tr class="table-bordered">'+
			                      '<td>Some-Type</td>'+
			                      '<td>Meal-Type</td>'+
			                      '<td>Available</td>'+
			                      '<td>£300</td>'+
			                      '<td><a href="#">Book Now</a></td>'+
			                    '</tr>'+
			                    '<tr class="table-bordered">'+
			                      '<td>Some-Type</td>'+
			                      '<td>Meal-Type</td>'+
			                      '<td>Available</td>'+
			                      '<td>£300</td>'+
			                      '<td><a href="#">Book Now</a></td>'+
			                    '</tr>';

			            html += '</table>'+
			                  '</div>'+
			                '</div>'+
			              '<div class="clearfix"></div>'+
			              '</div>';

	           	});

				html += '<div class="show-more">'+
			                  	'<span id="show-more-results"	><a href="#" class="results-link">Show More</a></span>'+
			                  '</div>';
			    //<a href="#" class="results-link">Show More</a>             	
	            $("#result").append(html);
	            $('#result .results').hide();
	            $('#result .results:lt(10)').show();
	            $('#result #loadingDiv #show_more_total').val('10');
	        }
	    });

	}

	// loading api data ...
	loadData(); 

	/* Show More */
	$(document).on("click", ".show-more #show-more-results", function(e){
		e.preventDefault();
		$("#result #loadingDiv").hide();
		var total = parseInt($("#result #loadingDiv #show_more_total").val())+10;
		$('#result #loadingDiv #show_more_total').val(total);
	    $('#result .results:lt('+total+')').show();
	});


});